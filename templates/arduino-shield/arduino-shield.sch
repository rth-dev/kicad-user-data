EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ardunino shield template"
Date "2020-12-07"
Rev ""
Comp "rth. engineering"
Comment1 "(c) 2020"
Comment2 ""
Comment3 "arduino compatible shield"
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 59D2CC43
P 1880 6720
F 0 "#PWR02" H 1880 6470 50  0001 C CNN
F 1 "GND" H 1880 6570 50  0000 C CNN
F 2 "" H 1880 6720 50  0001 C CNN
F 3 "" H 1880 6720 50  0001 C CNN
	1    1880 6720
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 59D2CC87
P 1880 6570
F 0 "#FLG03" H 1880 6645 50  0001 C CNN
F 1 "PWR_FLAG" H 1880 6720 50  0000 C CNN
F 2 "" H 1880 6570 50  0001 C CNN
F 3 "" H 1880 6570 50  0001 C CNN
	1    1880 6570
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 59D2DE83
P 980 6570
F 0 "#PWR05" H 980 6420 50  0001 C CNN
F 1 "+5V" H 980 6710 50  0000 C CNN
F 2 "" H 980 6570 50  0001 C CNN
F 3 "" H 980 6570 50  0001 C CNN
	1    980  6570
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG06
U 1 1 59D2E4EE
P 980 6720
F 0 "#FLG06" H 980 6795 50  0001 C CNN
F 1 "PWR_FLAG" H 980 6870 50  0000 C CNN
F 2 "" H 980 6720 50  0001 C CNN
F 3 "" H 980 6720 50  0001 C CNN
	1    980  6720
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR08
U 1 1 59D30A2F
P 1450 6570
F 0 "#PWR08" H 1450 6420 50  0001 C CNN
F 1 "+3.3V" H 1450 6710 50  0000 C CNN
F 2 "" H 1450 6570 50  0001 C CNN
F 3 "" H 1450 6570 50  0001 C CNN
	1    1450 6570
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG09
U 1 1 59D30D3A
P 1450 6720
F 0 "#FLG09" H 1450 6795 50  0001 C CNN
F 1 "PWR_FLAG" H 1450 6870 50  0000 C CNN
F 2 "" H 1450 6720 50  0001 C CNN
F 3 "" H 1450 6720 50  0001 C CNN
	1    1450 6720
	-1   0    0    1   
$EndComp
Wire Wire Line
	1880 6570 1880 6720
Wire Wire Line
	980  6570 980  6720
Wire Wire Line
	1450 6570 1450 6720
$Comp
L rth_connector:Conn_AVR_JTAG J3
U 1 1 59D5956F
P 3610 6660
F 0 "J3" H 3610 6970 50  0000 C CNN
F 1 "JTAG" H 3620 6350 50  0000 C CNN
F 2 "rth_pin-header:Pin_Header_Straight_2x05_Pitch2.54mm_JTAG_Front" H 3410 6660 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/JTAG" H 3410 6660 50  0001 C CNN
	1    3610 6660
	1    0    0    -1  
$EndComp
Wire Wire Line
	4010 6460 4210 6460
Wire Wire Line
	4210 6460 4210 6860
Wire Wire Line
	4210 6860 4010 6860
$Comp
L power:GND #PWR011
U 1 1 59D5BAD3
P 4210 6860
F 0 "#PWR011" H 4210 6610 50  0001 C CNN
F 1 "GND" H 4210 6710 50  0000 C CNN
F 2 "" H 4210 6860 50  0001 C CNN
F 3 "" H 4210 6860 50  0001 C CNN
	1    4210 6860
	1    0    0    -1  
$EndComp
NoConn ~ 3210 6760
Wire Wire Line
	4010 6660 4510 6660
Text Label 4510 6660 2    39   ~ 0
RST
$Comp
L power:+5V #PWR012
U 1 1 59D5D4E5
P 4310 6460
F 0 "#PWR012" H 4310 6310 50  0001 C CNN
F 1 "+5V" H 4310 6600 50  0000 C CNN
F 2 "" H 4310 6460 50  0001 C CNN
F 3 "" H 4310 6460 50  0001 C CNN
	1    4310 6460
	1    0    0    -1  
$EndComp
Wire Wire Line
	4010 6560 4310 6560
Wire Wire Line
	4310 6560 4310 6460
Text Label 2910 6460 0    39   ~ 0
TCK
Wire Wire Line
	3210 6460 2910 6460
Wire Wire Line
	3210 6560 2910 6560
Wire Wire Line
	3210 6660 2910 6660
Wire Wire Line
	3210 6860 2910 6860
Text Label 2910 6560 0    39   ~ 0
TDO
Text Label 2910 6660 0    39   ~ 0
TMS
Text Label 2910 6860 0    39   ~ 0
TDI
NoConn ~ 4010 6760
$Comp
L Device:R R8
U 1 1 59CCD346
P 8350 3610
F 0 "R8" V 8430 3610 50  0000 C CNN
F 1 "1k" V 8350 3610 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8280 3610 50  0001 C CNN
F 3 "" H 8350 3610 50  0001 C CNN
	1    8350 3610
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 1210 1500 1310
Wire Wire Line
	2300 1210 3000 1210
Wire Wire Line
	8800 2610 9600 2610
Wire Wire Line
	3480 2810 3700 2810
Wire Wire Line
	3270 3110 3700 3110
Wire Wire Line
	3270 2910 3700 2910
Wire Wire Line
	8600 3110 9600 3110
Wire Wire Line
	8700 3210 9600 3210
Wire Wire Line
	8700 4910 9600 4910
Wire Wire Line
	8600 4810 9600 4810
Wire Wire Line
	3100 2910 3270 2910
Wire Wire Line
	3000 3010 3700 3010
Wire Wire Line
	3000 3110 3000 3810
Wire Wire Line
	3000 3110 3270 3110
Text Label 9300 4610 0    39   ~ 0
D5_PWM
Text Label 9300 2710 0    39   ~ 0
IO13_PWM
Text Label 9300 4710 0    39   ~ 0
D4_A6
Text Label 2600 5110 0    39   ~ 0
A5
Text Label 2600 5010 0    39   ~ 0
A4
$Comp
L rth_button:SW_Reset SW1
U 1 1 59D58B1A
P 1950 1110
F 0 "SW1" H 1950 1320 50  0000 C CNN
F 1 "Reset" H 1950 940 50  0000 C CNN
F 2 "rth_button:Switch_SMD_Reset" H 1950 1410 50  0001 C CNN
F 3 "" H 1950 1410 50  0001 C CNN
	1    1950 1110
	1    0    0    -1  
$EndComp
Connection ~ 1500 1210
$Comp
L power:GND #PWR010
U 1 1 59D559F8
P 1500 1310
F 0 "#PWR010" H 1500 1060 50  0001 C CNN
F 1 "GND" H 1500 1160 50  0000 C CNN
F 2 "" H 1500 1310 50  0001 C CNN
F 3 "" H 1500 1310 50  0001 C CNN
	1    1500 1310
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1210 1700 1210
Wire Wire Line
	1500 1010 1500 1210
Wire Wire Line
	1700 1010 1500 1010
Wire Wire Line
	2200 1210 2300 1210
Connection ~ 2300 1210
Wire Wire Line
	2300 1010 2300 1210
Wire Wire Line
	2200 1010 2300 1010
Text Label 4000 1110 2    39   ~ 0
MOSI
Wire Wire Line
	3800 1010 4500 1010
Wire Wire Line
	4500 1010 4500 1710
Text Label 2800 1210 0    39   ~ 0
RST
Text Label 2800 1110 0    39   ~ 0
SCK
Text Label 2800 1010 0    39   ~ 0
MISO
Wire Wire Line
	3000 1010 2800 1010
Wire Wire Line
	2800 1110 3000 1110
Wire Wire Line
	3000 1710 2800 1710
Wire Wire Line
	3000 1810 2800 1810
Wire Wire Line
	2800 1910 3000 1910
Wire Wire Line
	3800 1110 4000 1110
Wire Wire Line
	3800 1210 4300 1210
Wire Wire Line
	3800 1910 4300 1910
Connection ~ 8800 2610
Wire Wire Line
	8800 2610 8800 3510
Wire Wire Line
	3800 1810 4000 1810
Connection ~ 3480 2810
Wire Wire Line
	3480 2420 3480 2810
Wire Wire Line
	3000 3010 3000 3110
Connection ~ 3270 3110
Wire Wire Line
	3270 3270 3270 3110
Connection ~ 3270 2910
Wire Wire Line
	3270 2420 3270 2910
Wire Wire Line
	4300 1210 4300 1910
Wire Wire Line
	3800 1710 4500 1710
Connection ~ 4500 1710
Connection ~ 4300 1910
Connection ~ 4300 3810
Connection ~ 8600 3110
Wire Wire Line
	8600 3110 8600 3310
Connection ~ 8700 3210
Wire Wire Line
	8700 3410 8700 3210
Wire Wire Line
	3000 3810 4300 3810
Wire Wire Line
	3100 3710 4500 3710
Wire Wire Line
	8100 3810 8200 3810
Wire Wire Line
	8200 3610 8100 3610
Connection ~ 8700 4910
Wire Wire Line
	8700 3610 8700 4910
Wire Wire Line
	8500 3610 8700 3610
Connection ~ 8600 4810
Wire Wire Line
	8600 3810 8600 4810
Wire Wire Line
	8500 3810 8600 3810
Connection ~ 3100 2910
Wire Wire Line
	3100 2910 3100 3710
Connection ~ 3000 3010
Connection ~ 3000 3110
Wire Wire Line
	9600 2310 8300 2310
Wire Wire Line
	8300 2410 9600 2410
Wire Wire Line
	9600 2510 8300 2510
Wire Wire Line
	8300 2610 8800 2610
Wire Wire Line
	9600 2710 8300 2710
Wire Wire Line
	8300 3110 8600 3110
Wire Wire Line
	8300 3210 8700 3210
Wire Wire Line
	2600 2610 3700 2610
Wire Wire Line
	2600 3210 3700 3210
Wire Wire Line
	2600 3110 3000 3110
Wire Wire Line
	2600 3010 3000 3010
Wire Wire Line
	2600 2910 3100 2910
Wire Wire Line
	2600 2810 3480 2810
Wire Wire Line
	2600 2710 3700 2710
Wire Wire Line
	2600 2510 3700 2510
Wire Wire Line
	2600 4910 3700 4910
Wire Wire Line
	2600 4810 3700 4810
Wire Wire Line
	2600 4710 3700 4710
Wire Wire Line
	2600 4610 3700 4610
Wire Wire Line
	8300 5110 9600 5110
Wire Wire Line
	8300 5010 9600 5010
Wire Wire Line
	8300 4910 8700 4910
Wire Wire Line
	8300 4810 8600 4810
Wire Wire Line
	8300 4610 9600 4610
$Comp
L rth_connector:Conn_AVR_ICSP J2
U 1 1 59D52090
P 3400 1110
F 0 "J2" H 3400 1310 50  0000 C CNN
F 1 "ICSP front" H 3400 910 50  0000 C CNN
F 2 "rth_pin-header:Pin_Header_Straight_2x03_Pitch2.54mm_ICSP_Front" H 3460 1110 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/In-system_programming" H 3460 1110 50  0001 C CNN
	1    3400 1110
	1    0    0    -1  
$EndComp
Text Label 2600 4910 0    39   ~ 0
TCK
Text Label 2600 4810 0    39   ~ 0
TMS
Text Label 2600 4710 0    39   ~ 0
TDO
Text Label 2600 4610 0    39   ~ 0
TDI
Text Label 2600 2610 0    39   ~ 0
IOREF
Text Label 2600 2710 0    39   ~ 0
RST
Text Label 2600 2910 0    39   ~ 0
5V
Text Label 2600 2810 0    39   ~ 0
3.3V
Text Label 2600 3210 0    39   ~ 0
Vin
Text Label 2600 3110 0    39   ~ 0
GND
Text Label 2600 3010 0    39   ~ 0
GND
Text Label 9300 4510 0    39   ~ 0
D6_PWM_A7
Text Label 9300 4410 0    39   ~ 0
D7
Text Label 9300 3210 0    39   ~ 0
IO8_A8
Text Label 9300 3110 0    39   ~ 0
IO9_PWM_A9
Text Label 9300 5110 0    39   ~ 0
TTL-Rx
Text Label 9300 5010 0    39   ~ 0
TTL-Tx
Text Label 9300 4910 0    39   ~ 0
I²C-SDA
Text Label 9300 4810 0    39   ~ 0
I²C-SCL
Text Label 9300 2610 0    39   ~ 0
GND
Text Label 9300 2510 0    39   ~ 0
AREF
Text Label 2800 1910 0    39   ~ 0
RST
Text Label 4000 1810 2    39   ~ 0
MOSI
Text Label 2800 1810 0    39   ~ 0
SCK
Text Label 2800 1710 0    39   ~ 0
MISO
$Comp
L power:+3.3V #PWR07
U 1 1 59D2EB12
P 3480 2420
F 0 "#PWR07" H 3480 2270 50  0001 C CNN
F 1 "+3.3V" H 3480 2560 50  0000 C CNN
F 2 "" H 3480 2420 50  0001 C CNN
F 3 "" H 3480 2420 50  0001 C CNN
	1    3480 2420
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 59D2D4EE
P 3270 2420
F 0 "#PWR04" H 3270 2270 50  0001 C CNN
F 1 "+5V" H 3270 2560 50  0000 C CNN
F 2 "" H 3270 2420 50  0001 C CNN
F 3 "" H 3270 2420 50  0001 C CNN
	1    3270 2420
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 59D2BE8D
P 3270 3270
F 0 "#PWR01" H 3270 3020 50  0001 C CNN
F 1 "GND" H 3270 3120 50  0000 C CNN
F 2 "" H 3270 3270 50  0001 C CNN
F 3 "" H 3270 3270 50  0001 C CNN
	1    3270 3270
	1    0    0    -1  
$EndComp
$Comp
L rth_connector:Conn_AVR_ICSP_Back_Mezzanine J1
U 1 1 59CFE63F
P 3400 1810
F 0 "J1" H 3400 2010 50  0000 C CNN
F 1 "ICSP back/mezz." H 3400 1610 50  0000 C CNN
F 2 "rth_pin-header:Pin_Socket_Straight_2x03_Pitch2.54mm_ICSP_Back" H 3460 1810 50  0001 C CNN
F 3 "https://en.wikipedia.org/wiki/In-system_programming" H 3460 1810 50  0001 C CNN
	1    3400 1810
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 59CCD455
P 8350 3810
F 0 "R9" V 8430 3810 50  0000 C CNN
F 1 "1k" V 8350 3810 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8280 3810 50  0001 C CNN
F 3 "" H 8350 3810 50  0001 C CNN
	1    8350 3810
	0    1    1    0   
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Header_[Digital_H] J11
U 1 1 59CC7EC6
P 8100 2810
F 0 "J11" H 8100 2210 50  0000 C CNN
F 1 "Digital_H_Male" H 8100 3310 50  0001 C CNN
F 2 "rth_arduino:Connector_PinHeader_2.54mm_[Digital_H]" H 8100 2810 50  0001 C CNN
F 3 "" H 8100 2810 50  0001 C CNN
	1    8100 2810
	1    0    0    1   
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Header_[Power] J5
U 1 1 59CC7B46
P 3900 2810
F 0 "J5" H 3900 3210 50  0000 C CNN
F 1 "Power_Male" H 3900 2310 50  0001 C CNN
F 2 "rth_arduino:Connector_PinHeader_2.54mm_[Power]" H 3900 2810 50  0001 C CNN
F 3 "" H 3900 2810 50  0001 C CNN
	1    3900 2810
	-1   0    0    -1  
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Header_[Analog_In] J7
U 1 1 59CC776F
P 3900 4810
F 0 "J7" H 3900 5110 50  0000 C CNN
F 1 "Analog_In_Male" H 3900 4410 50  0001 C CNN
F 2 "rth_arduino:Connector_PinHeader_2.54mm_[Analog_In]" H 3900 4810 50  0001 C CNN
F 3 "" H 3900 4810 50  0001 C CNN
	1    3900 4810
	-1   0    0    -1  
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Socket_[Digital_L] J8
U 1 1 59CBF706
P 9800 4710
F 0 "J8" H 9800 5110 50  0000 C CNN
F 1 "Digital L" H 9800 4210 50  0000 C CNN
F 2 "rth_arduino:Connector_PinSocket_2.54mm_[Digital_L]" H 9800 4710 50  0001 C CNN
F 3 "" H 9800 4710 50  0001 C CNN
	1    9800 4710
	1    0    0    -1  
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Header_[Digital_L] J9
U 1 1 59CBF4EC
P 8100 4810
F 0 "J9" H 8100 4310 50  0000 C CNN
F 1 "Digital_L_Male" H 8100 5210 50  0001 C CNN
F 2 "rth_arduino:Connector_PinHeader_2.54mm_[Digital_L]" H 8100 4810 50  0001 C CNN
F 3 "" H 8100 4810 50  0001 C CNN
	1    8100 4810
	1    0    0    1   
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Socket_[Power] J4
U 1 1 59CACF6C
P 2400 2810
F 0 "J4" H 2400 3210 50  0000 C CNN
F 1 "Power" H 2400 2310 50  0000 C CNN
F 2 "rth_arduino:Connector_PinSocket_2.54mm_[Power]" H 2400 2810 50  0001 C CNN
F 3 "" H 2400 2810 50  0001 C CNN
	1    2400 2810
	1    0    0    -1  
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Socket_[Analog_In] J6
U 1 1 59CACA56
P 2400 4810
F 0 "J6" H 2400 5110 50  0000 C CNN
F 1 "Analog In" H 2400 4410 50  0000 C CNN
F 2 "rth_arduino:Connector_PinSocket_2.54mm_[Analog_In]" H 2400 4810 50  0001 C CNN
F 3 "" H 2400 4810 50  0001 C CNN
	1    2400 4810
	1    0    0    -1  
$EndComp
$Comp
L rth_ardunio:Connector_Arduino_Socket_[Digital_H] J10
U 1 1 59CABCD2
P 9800 2710
F 0 "J10" H 9800 3210 50  0000 C CNN
F 1 "Digital H" H 9800 2110 50  0000 C CNN
F 2 "rth_arduino:Connector_PinSocket_2.54mm_[Digital_H]" H 9800 2710 50  0001 C CNN
F 3 "" H 9800 2710 50  0001 C CNN
	1    9800 2710
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3510 6900 3510
Wire Wire Line
	8100 3610 8100 3710
Wire Wire Line
	8100 3710 8100 3810
Connection ~ 8100 3710
Connection ~ 4500 3710
Wire Wire Line
	6900 3510 6900 3810
Wire Wire Line
	8300 2810 9600 2810
Wire Wire Line
	8300 4710 9600 4710
Wire Wire Line
	2600 5110 3700 5110
Wire Wire Line
	2600 5010 3700 5010
Wire Wire Line
	4300 1910 4300 3810
Wire Wire Line
	4500 1710 4500 3710
Wire Wire Line
	4500 3710 8100 3710
Wire Wire Line
	4300 3810 6900 3810
Wire Wire Line
	8300 2910 9600 2910
Wire Wire Line
	8300 3010 9600 3010
Wire Wire Line
	8300 4510 9600 4510
Wire Wire Line
	8300 4410 9600 4410
Text Label 9300 2810 0    39   ~ 0
IO12_A11
Text Label 9300 2910 0    39   ~ 0
IO11_PWM
Text Label 9300 3010 0    39   ~ 0
IO10_PWM_A10
$EndSCHEMATC
